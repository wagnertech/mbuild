cmake_minimum_required(VERSION 3.13)

#set (CMAKE_MODULE_PATH $ENV{CMAKE_MODULE_PATH})
# include toolset definition
#include(toolset OPTIONAL)
#include(toolset)

# libutil.a
FILE(GLOB SRC_FILES data/csrc/util/*.cpp)
add_library(util SHARED ${SRC_FILES})

# libhandledobject.so
FILE(GLOB SRC_FILES data/csrc/handledobject/*.cpp)
add_library(handledobject SHARED ${SRC_FILES})
target_include_directories(handledobject PRIVATE data/csrc/util)

# objecthandlerd
FILE(GLOB SRC_FILES data/csrc/objecthandlerd/*.cpp)
add_executable(objecthandlerdd ${SRC_FILES})
target_include_directories(objecthandlerdd PRIVATE data/csrc/util)
target_link_libraries(objecthandlerdd util)
target_link_libraries(objecthandlerdd handledobject)

