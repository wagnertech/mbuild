#!/bin/bash
set -e

base=$1

mkdir -p $base/usr/bin/
cp bin/* $base/usr/bin/

mkdir -p $base/usr/share/mbuild/
cp share/* $base/usr/share/mbuild/
cp copyright $base/usr/share/mbuild/

mkdir -p $base/usr/share/man/man7
pushd doc >/dev/null
	docbook2x-man mbuild.xml
	docbook2md mbuild.xml >../README.md
popd >/dev/null
gzip -c doc/mbuild.7 >$base/usr/share/man/man7/mbuild.7.gz
rm doc/mbuild.7
mkdir -p $base/usr/share/man/man1
gzip -c doc/mconfigure.1 >$base/usr/share/man/man1/mconfigure.1.gz

