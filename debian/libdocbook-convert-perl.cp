#!/bin/bash
set -e

pushd "$BUILD_DIR/Downloads/pm-Docbook-Convert" >/dev/null
	make test
	make install
popd >/dev/null

mkdir -p $1/usr/share/
rm -rf $1/usr/share/perl5 || true
mv $1/usr/lib/perl5 $1/usr/share/

