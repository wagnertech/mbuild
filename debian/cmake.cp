#!/bin/bash
set -e

paket=$1

mkdir -p $paket/opt/xeventhandler/
cp ~/build/objecthandlerdd $paket/opt/xeventhandler/objecthandlerd
cp ~/build/libhandledobject.so $paket/opt/xeventhandler/
cp ~/build/libutil.so $paket/opt/xeventhandler/

