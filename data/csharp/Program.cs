﻿// See https://aka.ms/new-console-template for more information

using System;
using mDoc;

class mDocMain{
    static int command_dispatch(string[] args) {

        // check runtime parameters
        if (args.Length < 2) throw new ApplicationException("mdoc needs 2 arguments.");
        
        string file = args[0];
        string format = args[1];
        
        return Worker.convert(file, format);
    }
    public static int Main(string[] argv){
        Console.WriteLine("Hello, World!");

        string usage = "mdoc <source> <target-format>";

        try {
            string[] argv1 = new string[argv.Length-1];
            for (int i = 1; i < argv.Length; i++) argv1[i-1] = argv[i];
            int ret = command_dispatch(argv1);
            if (ret != 0) {
                Console.WriteLine(usage);
            }
            return ret;
        }
        catch (Exception e) {
            Console.WriteLine(e.ToString());
            throw new Exception(e.ToString());
        }
    }
}
    
/*
# evaulate runtime parameters
if __name__ == '__main__':
    try:
        ret = command_dispatch(sys.argv[1:])
        if ret != 0:
            print (usage) 
        exit (ret)
    except Exception as e:
        print (e)
        print (usage)
        raise
*/
