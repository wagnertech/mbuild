/*
 * HandlerRecord.h
 *
 *  Created on: 11.03.2014
 *      Author: gnublin
 */

#ifndef HANDLERRECORD_H_
#define HANDLERRECORD_H_

#include "EDCommon.h"
#include "Event.h"

#include <iostream>
#include <fstream>
#include <map>
#include <vector>

class EDCommon;

class HandlerRecord {
private:
/*
	ifstream mInfile;

	struct record_t {
		char  hclass[20];		// handled class
		char  s1;
		char  inevent[10];		// incoming event
		char  s2;
		char  result[2];		// result of operation
		char  s3;
		char  ouevent[10];		// resulting outgoing event
		char  s4;
		char  def_data[20];		// Default event data
		char  s5;
		char  tclass[20];		// target class of outgoing event
		char  s6;
		char  tinst[2];			// target instance of outgoing event (0 for unspecified)
		char  s7;
		char  synchron;			// outgoing event synchron?
	};
*/
	struct outevent_t {
		int ouevent;
		int tclass;
		//char def_data[21];
		std::string def_data;
		char data_type;
		int tinst;
		bool synchron;
	};

	typedef vector<vector<map<int, vector<struct outevent_t> > > > record_event_t;
	// indices: hclass/inevent/result

	record_event_t events;

public:
	HandlerRecord(std::string filename, const EDCommon& );

	vector<Event> getEvents(Event& oldEvent, int opResult);
};

#endif /* HANDLERRECORD_H_ */
