/*
 * ConfigFile.h
 *
 *  Created on: 21.11.2016
 *      Author: gnublin
 */

#ifndef CONFIGFILE_H_
#define CONFIGFILE_H_

#include <string>
#include <vector>

/*
	ConfigFile reads a configuration file. The configuration file is expected with the following format:
[SectionName]
some data
for this section

empty lines are allowed

[OtherSection]
must not be empty
*/

class ConfigFile {
private:
	std::string fileName;

public:
	// Params: file name of the configuration file
	ConfigFile(std::string fn) : fileName(fn){}
	
	/*
		Returns all non empty lines of the given section.
		If the section is missing, an empty set is returned.
	*/
	std::vector<std::string> getSectionData(std::string section);
	
	// like getSectionData. If the section is missing, an exception is thrown.
	std::vector<std::string> requireSectionData(std::string section);
};

#endif /* CONFIGFILE_H_ */
