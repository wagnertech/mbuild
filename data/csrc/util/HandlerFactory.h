/*
 * HandlerFactory.h
 *
 *  Created on: 29.01.2014
 *      Author: gnublin
 */

#ifndef HANDLERFACTORY_H_
#define HANDLERFACTORY_H_

#include "EDCommon.h"
#include "HandledObject.h"

class HandlerFactory {
public:
	HandlerFactory(EDCommon& edCommon){
		this->edCommon = &edCommon;
		this->setClassIds();
	}
	/*
		Factory method to create user instance
		Params: tgtClass: index of the user class in the class list.
		                  This list is created at startup and depends on the
		                  configuration file.
		        tgtInst : id of instance, if there are specific instances
		                  e.g. at Timer.cpp
		Returns the requested object
		If the requested object cannot be created a EventException is thrown
	*/
	HandledObject& getInstance(int tgtClass, int tgtInst);
	const t_class_list& getClassList(){
		return this->edCommon->getClassList();
	}

private:
	EDCommon* edCommon;

	/*
		This function is called after startup and can be used to store the
		class ids in an additional map for performance reasons.
		If not used, provide an empty dummy implementation.
	*/
	void setClassIds();
};

#endif /* HANDLERFACTORY_H_ */
