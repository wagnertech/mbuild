/*
 * Attribute.h
 *
 *  Created on: 09.11.2021
 *      Author: debian10
 */

#ifndef ATTRIBUTE_H_
#define ATTRIBUTE_H_

#include "Event.h"
#include <string>
#include <ostream>

class Attribute {
public:
	// construct from string. Format see below.
	Attribute(std::string att_string);
	// construct from event entry (attribute name) + type + len + data
	//Attribute(std::string att_name, char typ, int len, const char* data_ptr);
	// construct from binary format (see below)
	Attribute(int len, const char* att_ptr);
	// construct from event containing the attribute value
	Attribute(std::string att_name, const Event& event)
		:att_name(att_name), att_event(event) {}
	// extract attribute from event
	Attribute(const Event& e):Attribute(e.getDataLength(),&e.getData()) {}

	// return event containing the attribute
	void fillDataIntoEvent(Event& evt);
	// fill attribute value from event
	void fillAttributeValue(const Event& evt);
	std::string getName();
	// return a Event containing the attribute value
	const Event& getEvent();
	// stream operator
	friend std::ostream& operator<<(std::ostream&,const Attribute&);

private:
	std::string att_name;
	Event att_event;
};

/* Attribute string format
 * Attribute defining string:type:attribute value
 */

/* Attribute binary format
 * length   : value
 * ---------:-----------------------------------------
 * 1        : length of attribute defining string (l1)
 * l1       : attribute defining string
 * 1        : type of attribute
 * int      : data length (l2)
 * l2       : data
 */

#endif /* ATTRIBUTE_H_ */
