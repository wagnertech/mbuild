/*
 * HandledObject.cpp
 *
 *  Created on: 27.12.2013
 *      Author: gnublin
 */

#include "HandledObject.h"
#include "EventException.h"

HandledObject::HandledObject()
: MAX_EVENT_STORE(3)
, event_store_idx(0) {
	this->event_store.resize(this->MAX_EVENT_STORE);
}

int HandledObject::create(Event& e, Event&) {
	throw EventException("create not implemented", e);
}
int HandledObject::set(Event& e) {
	throw EventException("set not implemented", e);
}
int HandledObject::get(Event& e, Event&) {
	throw EventException("get not implemented", e);
}
int HandledObject::trigger(Event& e, Event&) {
	throw EventException("trigger not implemented", e);
}
int HandledObject::response(Event& e, Event&) {
	throw EventException("response not implemented", e);
}
int HandledObject::deleteMe(Event& e, Event&) {
	throw EventException("delete not implemented", e);
}

void HandledObject::setEvent(const pair<int,Event>& evt) {
	this->event_store[this->event_store_idx] = evt;
	this->event_store_idx++;
	if (this->event_store_idx == this->MAX_EVENT_STORE) this->event_store_idx = 0;
}

bool HandledObject::getEvent(long thread, pair<int,Event>& evt) {
	for (int i=0; i<this->MAX_EVENT_STORE; i++) {
		if (this->event_store[i].second.getThread() == thread) {
			evt = this->event_store[i];
			return true;
		}
	}
	return false;
}

pair<int,Event> HandledObject::requireEvent(long thread) {
	pair<int,Event> evt;
	if (this->getEvent(thread, evt)) return evt;
	throw runtime_error("HandledObject: thread number not in event store.");
	return evt; // dummy statement
}

