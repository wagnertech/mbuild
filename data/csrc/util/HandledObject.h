/*
 * HandledObject.h
 *
 *  Created on: 27.12.2013
 *      Author: gnublin
 */

#ifndef HANDLEDOBJECT_H_
#define HANDLEDOBJECT_H_

#include "Event.h"

#include<vector>

class HandledObject {
public:
	HandledObject();

	virtual int create(Event&, Event&);
	virtual int set(Event&);
	virtual int get(Event&, Event&);
	virtual int trigger(Event&, Event&);
	virtual int response(Event&, Event&);
	virtual int deleteMe(Event&, Event&);

	void setEvent(const pair<int,Event>& evt);

	/* getEvent:
	 * true: if a event is stored to the given thread number.
	 *       on this case the event is removed from event_store
	 * false: if no event is stored to the given thread number.
	 */
	bool getEvent(long thread, pair<int,Event>& evt);

	/* requireEvent:
	 * if a event is stored to the given thread number, this event
	 * is returned. If no event is stored, an exception is thrown.
	 */
	pair<int,Event> requireEvent(long thread);

private:
	/* event store is organized as cyclic memory with MAX_EVENT_STORE
	 * entries.
	 */
	std::vector<pair<int, Event> > event_store;
	int MAX_EVENT_STORE; // const vertragt der ARM-Cmpiler nicht
	int event_store_idx;
};

#endif /* HANDLEDOBJECT_H_ */
