/*
 * util.h
 *
 *  Created on: 22.11.2016
 *      Author: gnublin
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <istream>
#include <string>
#include <sstream>
#include <vector>

#ifdef _ARMEL
// supplement std namespace
namespace std {
std::string to_string(int num);
}
#endif

namespace util {

std::string to_string(int num);

// splits a string into tokens
std::vector<std::string> split(std::string input, char token);

int stringToNumber(std::string str);

template<typename Item>
bool split(std::istream& s, char token, Item& item){
	s >> item;
	bool ret = !s.fail();
	char dummy;
	s >> dummy;
	return ret;
}

template<>
bool split(std::istream& s, char token, std::string& item);

#ifdef _ARMEL
	bool split(std::string&, char, std::string&, int&);
	bool split(std::string&, char, int&, float&);
	bool split(std::string&, char&, float&);
	bool split(std::string&, char, float&, float&);
	bool split(std::string&, char, std::string&, char&, std::string&);
	bool split(std::istream&, char token, std::string& str, size_t& st, int& i );
	bool split(std::istream&, char token, std::string& str1, std::string& str2, int& i );
	bool split(std::istream&, char, std::string&, std::string&, std::string&);
	bool split(std::istream&, char, std::string&, std::string&, std::string&, int&, bool&);
	bool split(std::istream&, char, std::string&, int&, bool&);
	bool split(std::istream&, char, int&, bool&);
#else
	template<typename First, typename... Rest>
	bool split(std::istream& s, char token, First& first, Rest& ... r){
		bool ret = split(s, token, first);
		if (ret) return split(s, token, r...);
		return false;
	}

	template<typename... Rest>
	bool split(std::string& s, char token, Rest& ... r){
		std::stringstream ss(s);
		return split(ss, token, r...);
	}

#endif

	template <typename T>
	T toVal(std::string s) {
		std::stringstream ss;
		T val;
		ss << s;
		ss >> val;
		return val;
	}
} /* namespace util */

#endif /* UTIL_H_ */
