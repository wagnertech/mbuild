/*
 * EventException.h
 *
 *  Created on: 25.06.2013
 *      Author: gnublin
 */

#ifndef EventException_H_
#define EventException_H_

#include <stdexcept>
#include "Event.h"

/* This exception is used for problems regarding a single event.
 * The deamon proceeds with the next event.
 */

class EventException : public std::runtime_error{
public:
	EventException(const char*);
	EventException(const char*, int);
	EventException(const char*, const Event& event);

	//virtual const char* what() const throw();

	bool hasEvent();
	const Event& getEvent(); // throws exception, if no event inside

private:
	//const char* msg;
	const Event* event;
};

#endif /* EventException_H_ */
