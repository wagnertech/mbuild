/*
 * util.cpp
 *
 *  Created on: 22.11.2016
 *      Author: gnublin
 */

#include "util.h"

#include <sstream>

using namespace std;

#ifdef _ARMEL
// supplement std namespace
std::string std::to_string(int num) {
	return util::to_string(num);
}
#endif

namespace util {

//Converting a number to string
string to_string(int num){
	ostringstream strout;
	string str;

	strout << num;
	str = strout.str();

	return str;
}

//Converting string to number
int stringToNumber(std::string str){
	std::stringstream  strin;
	int var;

	strin << str;
	strin >> var;

	return var;
}

vector<string> split(string input, char token) {
	stringstream ss;
	ss.str(input);
	string item;
	vector<string> elems;
	while (getline(ss, item, token)) {
		elems.push_back(item);
	}
	return elems;
}

template<>
bool split(std::istream& s, char token, std::string& item) {
	std::getline(s, item, token);
	return true; // if stream is empty, the empty string is returned.
}

#ifdef _ARMEL
bool split(std::string& s, char token, std::string& str, int& i) {
	stringstream ss;
	ss << s;
	bool ret = split(ss, token, str);
	if (!ret) return false;
	ret = split(ss, token, i);
	return ret;
}
bool split(std::string&s , char token, int& i, float& f) {
	stringstream ss;
	ss << s;
	bool ret = split(ss, token, i);
	if (!ret) return false;
	ret = split(ss, token, f);
	return ret;
}
bool split(std::string& s, char token, float& f) {
	stringstream ss;
	ss << s;
	bool ret = split(ss, token, f);
	return ret;
}
bool split(std::istream& s, char token, std::string& str, size_t& st, int& i ) {
	bool ret = split(s, token, str);
	if (!ret) return false;
	ret = split(s, token, st);
	if (!ret) return false;
	ret = split(s, token, i);
	return ret;
}
bool split(std::istream& s, char token, std::string& str1, std::string& str2, int& i ) {
	bool ret = split(s, token, str1);
	if (!ret) return false;
	ret = split(s, token, str2);
	if (!ret) return false;
	ret = split(s, token, i);
	return ret;
}
bool split(std::istream& s, char token, std::string& str1, std::string& str2, std::string& str3, int& i, bool& b) {
	bool ret = split(s, token, str1);
	if (!ret) return false;
	ret = split(s, token, str2);
	if (!ret) return false;
	ret = split(s, token, str3);
	if (!ret) return false;
	ret = split(s, token, i);
	if (!ret) return false;
	ret = split(s, token, b);
	return ret;
}
bool split(std::istream& s, char token, int& i, bool& b) {
	bool ret = split(s, token, i);
	if (!ret) return false;
	return split(s,token,b);
}
bool split(std::istream& s, char token, std::string& str, int& i, bool& b) {
	bool ret = split(s, token, str);
	if (!ret) return false;
	ret = split(s,token,i);
	if (!ret) return false;
	return split(s,token,b);
}
bool split(std::istream& s, char token, std::string& str1, std::string& str2, std::string& str3) {
	bool ret = split(s, token, str1);
	if (!ret) return false;
	ret = split(s,token,str2);
	if (!ret) return false;
	return split(s,token,str3);
}
bool split(std::string& s, char token, float& f1, float& f2) {
	stringstream ss;
	ss << s;
	bool ret = split(ss, token, f1);
	if (!ret) return false;
	return split(ss,token,f2);
}
bool split(std::string& s, char token, std::string& s1, char& c, std::string& s2) {
	stringstream ss;
	ss << s;
	bool ret = split(ss, token, s1);
	if (!ret) return false;
	ret = split(ss, token, c);
	if (!ret) return false;
	return split(ss,token,s2);
}

#endif

} /* namespace util */
