/*
 * DistributionRecord.h
 *
 *  Created on: 11.03.2014
 *      Author: gnublin
 */

#ifndef DISTRIBUTIONRECORD_H_
#define DISTRIBUTIONRECORD_H_

#include <vector>
#include <string>
#include "HandlerRecord.h"

class DistributionRecord {

private:
	typedef std::vector<std::vector<int> > distrib_t;
	// indices: tclass/tinst

	distrib_t distrib;

public:
	// during executing this construcor the class list is filled
	DistributionRecord(std::string filename, std::vector<std::string>& class_list);

	int getTgtHandler(int tgtClass, int tgtInst);

	//std::vector<std::string> getClassList() const;
};

#endif /* DISTRIBUTIONRECORD_H_ */
