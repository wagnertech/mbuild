/*
 * Trigger.h
 *
 * sets status to true, if current value exceeds upper threshold -> return value 1
 * sets status to false, if current value lowers lower threshold -> return value -1
 */

#ifndef TRIGGER_H_
#define TRIGGER_H_

#include <HandledObject.h>

#include "EDCommon.h"

class Trigger: public HandledObject {
public:
	/*
	 * Fabric method.
	 * Trigger instances are defined in the configuration file:
	 * [Trigger]
	 * lower;upper
	 */
	static Trigger& getInstance(const EDCommon&, int id);
	Trigger (float l, float u): status(false), lower(l), upper(u) {}
	/*
	 * set method.
	 * Expected data: float with current value
	 * Returns: see class description
	 */
	virtual int set(Event&); // override;

private:
	static std::vector<Trigger> instances;
	bool status;
	float lower;
	float upper;
};

#endif /* TRIGGER_H_ */
