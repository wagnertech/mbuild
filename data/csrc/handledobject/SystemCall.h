/*
 * SystemCall.h
 *
 * On trigger event the given string data is executes as system call.
 *
 */

#ifndef SYSTEMCALL_H_
#define SYSTEMCALL_H_

#include <HandledObject.h>

class SystemCall: public HandledObject {

public:
	// static instance instantiated at system start
	static SystemCall theInstance;

	/*
	 * trigger performs a system call.
	 * Expected data: String with the command line
	 * Returns: The return value of the system call
	 */
	virtual int trigger(Event&, Event&);

};

#endif /* SYSTEMCALL_H_ */
