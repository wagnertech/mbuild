/*
 * DataProvider.cpp
 *
 *  Created on: 09.04.2017
 *      Author: gnublin
 */

#include "DataProvider.h"

#include "ConfigFile.h"
#include "EventException.h"
#include "util.h"

#include <fstream>
#include <memory.h> // memcpy
#include <iostream>
#include <stdexcept>

using namespace std;

vector<DataProvider> DataProvider::instances;

DataProvider& DataProvider::getInstance(const EDCommon& edCommon, int id) {
	if (DataProvider::instances.size() == 0) {
		// load instances

		// get config file name
		string fileName = edCommon.getConfigFileName();

		// get data provider content
		ConfigFile cf(fileName);
		vector<string> lines = cf.requireSectionData("DataProvider");
		for (size_t i=0; i<lines.size(); i++) {
			int max_delay;
			util::split(lines[i], ';', fileName, max_delay);
			DataProvider::instances.push_back(DataProvider(i+1, max_delay, fileName));
		}
	}
	if (id<1 || id>DataProvider::instances.size()) throw EventException("DataProvider-Id out of range", id);
	return DataProvider::instances[id-1];
}

DataProvider::DataProvider(int id, int delay, const std::string& fileName)
	: id (id)
	, last_update(0)
	, max_delay(delay) {
	ifstream file(fileName.c_str()); // .c_str is needed for ARM
	if (!(file.good())) throw runtime_error(string("cannot open file ")+fileName);

	string line;
	while (getline(file, line)) {
		int in_val;
		float out_val;
		bool suc = util::split(line, ';', in_val, out_val);
		if (!suc) throw runtime_error("Invalid data line in "+fileName+": "+line);
		this->calib_data.push_back(pair<int,float>(in_val, out_val));
	}
}

int DataProvider::get(Event& evt, Event& out) {
	if (time(0)-this->last_update > max_delay) return this->id;
	out.pushData((char*) &this->last_val, sizeof(float), 'f');
	return 0;
}

int DataProvider::response(Event& evt, Event& out) {
	int req_data;
	memcpy(&req_data, &(evt.getData()), sizeof(int));
	float out_data = 0.;
	bool value_found = false;
	size_t i = 0;
	while (!value_found && i<this->calib_data.size()) {
		if (this->calib_data[i].first >= req_data) {
			if (i == 0) {
				// untere extrapolation
				out_data = calib_data[0].second -
					(calib_data[1].second - calib_data[0].second) /
					((float)(calib_data[1].first - calib_data[0].first)) *
					(calib_data[0].first - req_data);
				value_found = true;
			}
			else {
				// interpolation
				out_data = calib_data[i-1].second +
					(calib_data[i].second - calib_data[i-1].second) /
					((float)(calib_data[i].first - calib_data[i-1].first)) *
					(req_data - calib_data[i-1].first);
				value_found = true;
			}
		}
		i++;
	}
	if (!value_found) {
		// obere extrapolation
		i = calib_data.size()-1;
		out_data = calib_data[i].second +
			(calib_data[i].second - calib_data[i-1].second) /
			((float)(calib_data[i].first - calib_data[i-1].first)) *
			(req_data - calib_data[i].first);

	}
	out.pushData((char*)(&out_data), sizeof(out_data), 'f');
	return 0;
}
