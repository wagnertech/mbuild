/*
 * SystemCall.cpp
 *
 *  Created on: 23.12.2017
 *      Author: gnublin
 */

#include "SystemCall.h"

#include <cstdlib>

int SystemCall::trigger(Event& e, Event&) {
	char* cmd = &e.getData();
	return system(cmd);
}

SystemCall SystemCall::theInstance;
