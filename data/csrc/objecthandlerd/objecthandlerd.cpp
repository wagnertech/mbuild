/*
 * objecthandlerd.cpp
 *
 *  Created on: 27.12.2013
 *      Author: gnublin
 */


//#include <sys/types.h>
//#include <sys/ipc.h>
//#include <sys/msg.h>
//#include <fstream>
//#include <errno.h>
#include <string.h>
#include <iostream>
#include <stdlib.h> // for atoi
#include <memory> // auto_ptr

#include "Attribute.h"
#include "Config.h"
#include "DistributionRecord.h"
#include "EDCommon.h"
#include "Event.h"
#include "HandlerFactory.h"
#include "HandlerRecord.h"
#include "EventException.h"

using namespace std;

int main(int argc, char *argv[])
{
	// check parameters
	const char* usage = "objecthandlerd <incarnation> [-c config file] [-q queue name] [-- other params]";
	if(argc == 1) {
		std::cerr << "parameter missing. usage: " << usage << std::endl;
		return 1;
	}

	// TODO: atoi ersetzen
	long incarnation = atoi(argv[1]);

	int p_i = 2;

	string configFileName;
	string queueName;
	Config* config = Config::getInstance();

	while (p_i < argc){
		if(strcmp(argv[p_i],"-c")==0){
			configFileName = argv[p_i+1];
			p_i += 2;
		}
		else if (strcmp(argv[p_i],"--")==0){
			p_i++;
			while (p_i < argc){
				if (p_i+1 < argc) {
					string key(argv[p_i]);
					string value(argv[p_i+1]);
					config->add(key, value);
					p_i += 2;
				}
				else {
					cerr << incarnation << " - invalid parameter count." << endl;
					cout << "usage: " << usage << endl;
					return 1;
				}
			}
		}
		else {
			cerr << incarnation << " - invalid parameter " << argv[p_i] << endl;
			cout << "usage: " << usage << endl;
			return 1;
		}
	}

	// define buffer
	t_message qbuf;

	try {

		// get helper class
		EDCommon edCommon(configFileName, queueName);

		// create handled object
		HandlerFactory hf = HandlerFactory(edCommon);

		cout << incarnation << " - object handler starting ..." << std::endl;

		// open queue
		edCommon.openQueue();

		for(;;) {
			try {

				// get event
				edCommon.getEvent(qbuf, incarnation);

				// create event object
				#ifdef _ARMEL
				std::auto_ptr<Event> event(new Event(qbuf.event));
				#else
				std::unique_ptr<Event> event(new Event(qbuf.event));
				#endif

				cout << incarnation << " - Event received: " << *event << endl;

				// create target object
				HandledObject& ho(hf.getInstance(event->getTgtClass(), event->getTgtInst()));

				Event out_event;
				int ret = -1;
				switch (event->getEventId()) {
					case Event::EVT_RESPONSE: ret = ho.response(*event, out_event);
						break;
					case Event::EVT_CREATE: ret = ho.create(*event, out_event);
						break;
					case Event::EVT_GET: ret = ho.get(*event, out_event);
						break;
					case Event::EVT_SET: ret = ho.set(*event);
						break;
					case Event::EVT_DELETE: ret = ho.deleteMe(*event, out_event);
						break;
					case Event::EVT_TRIGGER: ret = ho.trigger(*event, out_event);
				}

				cout << incarnation << " - Event execution returned: " << ret << endl;

				// build response event
				int tgtHandler;
				if (event->getEventId() ==  Event::EVT_RESPONSE && ret == 0) {
					// search for further response
					pair<int,Event> incEvent;
					if (ho.getEvent(event->getThread(), incEvent)) {
						// restore old event with new data and old incarnation
						qbuf.sndIncarnation = incEvent.first;
						*event = incEvent.second;
					}
				}
				if (event->isSynchron() && ret == 0) {
					out_event.setSndClass	(event->getTgtClass());
					out_event.setSndInst		(event->getTgtInst());
					out_event.setTgtClass	(event->getSndClass());
					out_event.setTgtInst		(event->getSndInst());
					out_event.setEvtId		(Event::EVT_RESPONSE);
					out_event.setThread		(event->getThread());
					out_event.setSynchon		(false);
					tgtHandler = qbuf.sndIncarnation;

					cout << incarnation << " - Sending event to incarnation: " << tgtHandler << endl;

					// send new event
					qbuf.tgtIncarnation = tgtHandler;
					qbuf.sndIncarnation = incarnation;
					memcpy(&qbuf.event, &(out_event.getEventStruct()), sizeof(EventStruct));
					edCommon.sendEvent(qbuf);
				}
				else {
					if (event->isSynchron()) {
						// store event in eventstore
						ho.setEvent(pair<int,Event>(qbuf.sndIncarnation, *event));
					}
					// find subsequent events
					HandlerRecord& handlerRecord = edCommon.getHandlerRecord();
					std::vector<Event> nextEvents = handlerRecord.getEvents(*event.get(), ret);

#ifdef _ARMEL
					// no C++11 support
					for(std::vector<Event>::iterator it=nextEvents.begin(); it<nextEvents.end(); it++){
						Event event = *it;
#else
					// send them
					for (auto& event : nextEvents) {
#endif

						// fill data, if necessary
						if // event processing returned data
							(out_event.getDataLength() > 0) {
							if // no static data in subsequent event
								(event.getDataLength() == 0) {
								// use this event data
								event.pushData(&(out_event.getData()), out_event.getDataLength(), out_event.getDataType());
							}
							else if // subsequent event expects attribute data
								(event.getDataType() == 'a') {
								Attribute att(event.getDataLength(), &event.getData());
								att.fillAttributeValue(out_event);
								// fill attribute into next event
								att.fillDataIntoEvent(event);
							}
						}

						// find instance for new event
						DistributionRecord& distrib = edCommon.getDistributionRecord();
						tgtHandler = distrib.getTgtHandler(event.getTgtClass(), event.getTgtInst());

						cout << incarnation << " - Sending event to incarnation: " << tgtHandler << endl;

						// send new event
						qbuf.tgtIncarnation = tgtHandler;
						qbuf.sndIncarnation = incarnation;
						memcpy(&qbuf.event, &(event.getEventStruct()), sizeof(EventStruct));
						edCommon.sendEvent(qbuf);
					}
				}
			}
			catch (EventException& ee) {
				cerr << incarnation << " - Event exception: " << ee.what() << endl;
				if (ee.hasEvent()) cerr << ee.getEvent() << endl;
				cerr << incarnation << " - Proceeding with next event ..." << endl;
			}
		}
	}
	catch(std::exception& e){
		std::cerr << incarnation << " - setup error: " << e.what() << std::endl;
		return 1;
	}
	catch(...) {
		std::cerr << incarnation << " - unknown error." << std::endl;
	}
}


