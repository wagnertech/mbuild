#!/bin/bash

# util routines for a C/C++ - build

function check_arch {
	case $1 in
	armel) ;;
	i386) ;;
	*)	echo "unknown architecture $1"
		exit 1
	esac
}

function install_cpp_make {
# $1: dir to install
# $2 (opt): if set to "lib" the fPIC flag is added

	# find makefile
	if [ -f /usr/share/mbuild/cpp.make ]
	then
		makefile=/usr/share/mbuild/cpp.make
	elif [ -f $cwd/projects/tools/make/cpp.make ]
	then
		makefile=$cwd/projects/tools/make/cpp.make
	else
		echo "cannot find cpp makefile." >&2
		exit 1
	fi
	src=$(pwd);
	pushd $1
	if [ -f makefile ]; then rm makefile; fi
	if [ -L makefile ]; then rm makefile; fi
	if [ -f make.post ]; then rm make.post; fi
	ln -s $makefile makefile
	echo "SOURCE = \\" > make.pre
	for file in $(ls *.cpp)
	do
		echo "  $file \\" >> make.pre
	done
	echo >> make.pre
	if [ "$DEB_HOST_ARCH" != "$DEB_BUILD_ARCH" ]
	then
		#cross compiling
		echo "PREFIX = $DEB_HOST_GNU_TYPE-" >> make.pre
		echo "SYSROOT = --sysroot=$HOME/cross" >> make.pre
	fi

	if [ "$ARCH" = "armel" ]
	then
		echo "CXX = arm-linux-gnueabi-g++" >> make.pre
		echo "CC = arm-linux-gnueabi-g++" >> make.pre
		echo "CXXFLAGS += -D_ARMEL" >> make.pre
		echo 'export PATH := /opt/eldk-5.0/armv5te/sysroots/i686-oesdk-linux/usr/bin/armv5te-linux-gnueabi/:/opt/eldk-5.0/armv5te/sysroots/i686-oesdk-linux/bin/armv5te-linux-gnueabi/:$(PATH)' >> make.pre
	else
		echo "CXXFLAGS += -std=c++0x" >> make.pre
	fi
	
	# compilation of libraries need fPIC flag
	if [ "$2" == "lib" ]
	then
		echo "CXXFLAGS += -fPIC" >> make.pre
	fi

	popd
}

function append_dependency_common {
# common part of append_dependency and append_library
# parameter:
# $1: main directory
# $2: dependency directory
# $3: dependency artefact

	src=$(pwd)
	echo "DEPS += $3" >> $1/make.pre
	echo "$3:" >> $1/make.post
	echo "	cd $src/$2 && make TARGET=$3" >> $1/make.post
}

function append_dependency {
# adds another drectory, where a c++ compile is performed
	append_dependency_common $*
	echo "" >> $1/make.post
}

function add_include {
# adds a include directory
# parameter:
# $1: main directory
# $2: include dir

	src=$(pwd)
	echo "CXXFLAGS += -I$src/$2" >> $1/make.pre
}

function add_library {
# adds a extern library
# parameter:
# $1: main directory
# $2: library name (without l or lib )
# $3: library path (optional)

	src=$(pwd)
	echo "EXTLIB += -l$2" >> $1/make.pre
	if [ -n "$3" ]
	then
		lib_path=$3
		if [ ${lib_path:0:1} != "/" ]
		then
			lib_path="$src/$lib_path"
		fi
		echo "LDFLAGS += -L$lib_path" >> $1/make.pre
	fi
}

function append_library {
# same as append_dependency. In addition a include statement + a link to the library is added to "main directory"
# parameter:
# $1: main directory
# $2: dependency directory
# $3: dependency artefact

	append_dependency_common $*
	echo "	ln -sf $src/$2/$3 ." >> $1/make.post
	echo "" >> $1/make.post

	echo "LDLIBS += $3" >> $1/make.pre
	echo "CXXFLAGS += -I$src/$2" >> $1/make.pre

}

