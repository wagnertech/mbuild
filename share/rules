#!/usr/bin/make -f

# Generic rules for mBuild build process

# defile default operations
NOP = @echo "No operation for target $@"
DEB = fakeroot dpkg-deb $(DEB_OPTS) --build debian/tmp
INSERT_BUILD = /usr/share/mbuild/insert_build.sh
RSYNC_OPT = -av --exclude="build" --exclude="debian/tmp" --exclude="Packages" --exclude="*Aktuell" --exclude=".*" --exclude="Makefile" --exclude="configure"
BUILD_DIR = .

std_inst_tgt = debian/tmp/usr/share/doc/$(paket)/copyright debian/tmp/usr/share/doc/$(paket)/README.debian
changelog_source = debian/$(paket).changelog

include debian/rules.pre

.PHONY: build

default: $(BUILD)

# Löschen aller kompilierten, erzeugten und nicht benötigten Dateien im Bauverzeichnis
clean:
	-rm -rf debian/tmp

# Bauen der kompilierten Programme und formatierten Dokumente aus den Quellen im Bauverzeichnis
build: $(BUILD_DIR)/debian/$(paket).build sync
	cd $(BUILD_DIR) ; debian/$(paket).build
	# Build succeeded

nobuild:
	# No build required

install:
	debian/$(paket).cp $(INSTALL_DIR)

pack: $(PACK)
	# packing succeeded

# Bauen der kompilierten architekturabhängigen Programme aus den Quellen im Bauverzeichnis
build-arch:

# Bauen der architekturunabhängigen formatierten Dokumente aus den Quellen im Bauverzeichnis
build-indep:

# Erstellen aller Binärpakete (effektiv ist dies die Kombination der binary-arch- und binary-indep-Ziele)
binary: binary-arch binary-indep 

# Erstellen Architektur-abhängiger (Architecture: any) Binärpakete im übergeordneten Verzeichnis
binary-arch: $(BINARY_ARCH)

# Erstellen Architektur-unabhängiger (Architecture: all) Binärpakete im übergeordneten Verzeichnis
binary-indep: $(BINARY_INDEP)

version: $(BUILD_DIR)/$(TARGET)
	$(INSERT_BUILD) $(BUILD_DIR)/$(TARGET) $(build)
	cp $(BUILD_DIR)/*$(build)* ../

$(BUILD_DIR)/$(TARGET): $(BUILD_DIR)/$(COMPILE_TARGET)
	cp $(BUILD_DIR)/$(COMPILE_TARGET) $(BUILD_DIR)/$(TARGET)

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

sync: $(BUILD_DIR)
	rsync $(RSYNC_OPT) ./ $(BUILD_DIR)/

copy: debian/$(paket).cp install
	create-conffiles debian/tmp
	touch debian/tmp

../$(paket)_$(version)-$(build)_$(arch).deb:debian/tmp/DEBIAN/control $(std_inst_tgt) $(add_inst_tgt) debian/tmp
	$(DEB)
	mv debian/tmp.deb ../$(paket)_$(version)-$(build)_$(arch).deb

debian/tmp/DEBIAN/control: debian/control debian/changelog
	dpkg-gencontrol

debian/control: debian/$(paket).control
	sed "s/_DEB_HOST_ARCH/${DEB_HOST_ARCH}/" debian/$(paket).control > debian/control

debian/changelog: $(changelog_source) debian/rules.pre
	sed "s/%BUILD%/$(build)/" $(changelog_source) |sed "s/%VERSION%/$(version)/" > debian/changelog

debian/tmp/usr/share/doc/$(paket)/changelog: debian/changelog
	mkdir -p debian/tmp/usr/share/doc/$(paket)
	cp debian/changelog debian/tmp/usr/share/doc/$(paket)/changelog

debian/tmp/usr/share/doc/$(paket)/copyright: debian/copyright
	mkdir -p debian/tmp/usr/share/doc/$(paket)
	cp debian/copyright debian/tmp/usr/share/doc/$(paket)/copyright

debian/tmp/usr/share/doc/$(paket)/README.debian: debian/README.debian
	mkdir -p debian/tmp/usr/share/doc/$(paket)
	cp debian/README.debian debian/tmp/usr/share/doc/$(paket)/README.debian

debian/tmp/DEBIAN/preinst: debian/$(paket).preinst
	cp debian/$(paket).preinst debian/tmp/DEBIAN/preinst

debian/tmp/DEBIAN/postinst: debian/$(paket).postinst
	cp debian/$(paket).postinst debian/tmp/DEBIAN/postinst

debian/tmp/DEBIAN/prerm: debian/$(paket).prerm
	cp debian/$(paket).prerm debian/tmp/DEBIAN/prerm

debian/tmp/DEBIAN/postrm: debian/$(paket).postrm
	cp debian/$(paket).postrm debian/tmp/DEBIAN/postrm

debian/README.debian:
	echo "developed under:" > debian/README.debian
	cat /etc/issue >> debian/README.debian
	echo "Architecture: $(arch)" >> debian/README.debian

debian/copyright:
	cp /usr/share/mbuild/copyright debian/copyright

debian/default.changelog:
	@echo "$(paket) ($(version)-$(build)) unstable; urgency=medium" >debian/default.changelog
	@echo "  * generated" >>debian/default.changelog
	@echo " -- Michael Wagner <michael@wagnertech.de>  Thu, 11 Feb 2021 10:00:00 +0100" >>debian/default.changelog

tgz: debian/$(paket).zcp ../$(paket)_$(version)-$(build).tgz

../$(paket)_$(version)-$(build).tgz: debian/tmp
	debian/$(paket).zcp $(INSTALL_DIR)
	cd debian/tmp; tar czf ../../../$(paket)_$(version)-$(build).tgz *

zip: debian/$(paket).zcp ../$(paket)_$(version)-$(build).zip

../$(paket)_$(version)-$(build).zip: debian/tmp
	debian/$(paket).zcp $(INSTALL_DIR)
	cd debian/tmp; zip -r ../../../$(paket)_$(version)-$(build).zip *

# Load project specification
-include rules.post


