# Generic cpp makefile for mBuild build process

# default parameters
CC = $(PREFIX)g++
CXX = $(PREFIX)g++

include make.pre

# All Target
all: $(DEPS) $(SOURCE:%.cpp=%.o) $(TARGET)

# Other Targets
clean:
	-rm *.o
	-rm $(TARGET)

.PHONY: all make.post

%.a: $(SOURCE:%.cpp=%.o)
	ar r $(TARGET) *.o

%.so: $(SOURCE:%.cpp=%.o) $(LDLIBS)
	echo $(PATH)
	$(CXX) -shared $(SYSROOT) -o $(TARGET) *.o $(LDFLAGS) $(LDLIBS) $(EXTLIB)
-include make.post
