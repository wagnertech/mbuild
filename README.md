WagnerTech Utilities

# mbuild 7 #

# NAME #

mbuild - build system of WagnerTech UG

# SYNOPSIS #

`mbuild`

# Description #

mbuild provides a build system based on make, git, subversion. The build process consists of 3 phases: configuration \- compile \- pack

This sequence can either be called step by step or by the `mconfigure` script. In that case the parameters for configure and make are defined in the PAKET.conf file. The format of this file is shown in the FILES section, for the parameters of  `mconfigure` see mconfigure(1).

* **configuration**

    There is a standard configuration script located in /usr/share/mbuild. If no project defined configuration script is present, this is linked into the project dir. For the parameters
 of this script run  `mconfigure`  without parameters.

    `./configure`  performs the following tasks:

    Determination of version and build number. The version number can be set in a  `PAKET.changelog`  file, can be set by the \-v option of `mconfigure`  or be set by the project itself in the `PAKET.prepare`  script. The build number is always taken from the CM system: With SVN it is the global version number, with GIT the build number is taken from the tag. On build a tag in the form project_version-build is expected. If not present a tag in the form v_version-build is searched. 

 Version may be 0.0 if set elsewhere. In that case  `PAKET.changelog`  should contain  %VERSION%-%BUILD%  as verion/build.

    If a  `PAKET.prepare`  file is present in the debian directory, it is called in the project directory.

    If a  `PAKET.prebuild`  file is present in the debian directory, it is called in the build directory.

* **compile**

    To perform the compile step a  `PAKET.build`  script or a `PAKET.cmake`  file is needed in the  `debian`  directory.  `./configure`  creates  `PAKET.build`  for compile option PDF and CPP.

* **pack**

    The following targets types are supported:

    *DEBIAN:*  In that case a  `PAKET.cp`  script and a `PAKET.control`  file are needed in the debian directory. If pre/post installation scripts are needed, these files must be also placed in the  `debian`  directory.

    A standard copyright file is included. An user defined file can be used by copying it to debian/copyright in the PAKET.prepare script.

    If files are present to be placed unter  `/etc`  a conffiles is creates to avoid changes made by the administrator.

    If there are additional options for the `dpkg-deb` command (like `-Zxz` for `xz` compression) export them in the `DEB_OPTS` environment var.

    *ZIP/TGZ:*  In that case a  `PAKET.cp`  is needed. The files have to be copied into  $1/... . All directories/files below are zipped/tgzed.

    *FILE:*  The  compile_target  is copied into the base dir, the build number is included into the file name. If the  target_name  parameter is set the artifact is renamed into that name.

# FILES #

* **PAKET.conf**

    Configuration file of  `mconfigure` . The following parameters can be set:

    *add_conf_args*  Additional arguments provided to  `./configure.`

    *build_replacement*  Corresponds to \-B option of  `./configure.`

    *compile_dir*  Directory relative to project directory where `make`  is called. Corresponds to \-d option of  `./configure.`

    *compile_target*  Target provided to  `make` . Corresponds to \-e option of  `./configure` .

    *compile_type*  =ANY|PDF|CPP|NONE \(mandatory). Corresponds to \-c option of `./configure` .

    *multibuild=n*  If set, the whole build run is performed n times. As example see  `pdf.conf`  in the EXAMPLE section.

    *make_par*  Additional parameters provided to  `make` .

    *run*  This variable is set to the run number \(starting by 1) in case of a multirun configuration. See  `pdf.conf`  in the EXAMPLE section.

    *target_name*  Corresponds to \-n option of  `./configure.`

    *target_type*  Corresponds to \-t option of  `./configure.`

* **PAKET.changelog**

    Example: 

    projects (0.2-%BUILD%) unstable; urgency=medium
      * Build in svn-Umgebung
     -- Michael Wagner <michael@wagnertech.de>  Fr 10. Aug 22:54:48 CEST 2018

* **PAKET.prepare**

    Any executable script file executed in configure step

* **PAKET.prebuild**

    Any executable script file executed in configure step in the  `~/build`  directory.

* **PAKET.build**

    Any executable script file executing the build in the  `~/build`  directory.

* **PAKET.cmake**

    cmake file \( `CMakeLists.txt` ) for building C/C++ code. If a cross compile option is set in the mconfigure script, according definitions are included.

* **PAKET.cp**

    An executable script file copying the artefacts to target paths. This script is called by `make install` . The target paths have to be preposed by $1: 

    cp ~/build/my_artefact $1/usr/bin/

* **PAKET.preinst PAKET.postint PAKET.prerm PAKET.postrm**

    Optional pre/post installation scripts

* **PAKET.control**

    Debian control file

These files are expected in a  `debian`  directory, placed below the projects main directory.

# EXAMPLES #

In this repository in the  `data`  directoty the are several example builds. The correspondig \.conf filed are placed in the  `debian`  directory.

* **any.conf**

    Custom build, executing the files  `any.prepare` ,  `any.prebuild`  and  `any.build` .

* **cmake.conf**

    Performing a cmake build as defined in  `cmake.cmake` .

* **csharp.conf**

    Like  `any.conf` , performing a C# comile with the mono compiler.

* **doc_class.conf**

    Compile, using an environment variable \(here  DOC_CLASS ), converting XML to TEX with  `mdoc`  and PDF compile using  `pdflatex`

* **libdocbook-convert-perl.conf**

    Build of a perl module using  `cpan` .

* **pdf.conf**

    Compile of a PDF document with  `pdflatex` . This conf file also holds an example performing multiple build runs using the  multibuild  and  run  variables.

* **tgz.conf**

    Build packing a TGZ and ZIP file.

# SEE ALSO #

make(1), mconfigure(1)

# BUGS #

No known bugs.

# AUTHOR #

Michael Wagner \(michael@wagnertech.de)

